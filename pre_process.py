#! /usr/bin/env python
#coding:utf-8

import pprint
import json
import corenlp
import re
import time

def remove_dust(sentence):
    # "\n"を除去
    sentence = sentence.replace("\n","")
    # "."を除去
    sentence = sentence.replace(".","")
    # ","をなくす
    sentence = sentence.replace(",","")
    # ":"をなくす
    sentence = sentence.replace(":","")
    # """をなくす
    sentence = sentence.replace("\"","")
    # "(",")"を空白に
    sentence = sentence.replace("("," ")
    sentence = sentence.replace(")"," ")
    #複数の空白を一つの空白に
    sentence = sentence = re.sub(r"\s+"," ",sentence)
    return sentence
if __name__ == "__main__":
    #ファイル読み込み
    print("ファイル読み込み" + str(time.time()))
    f = open("text_set/dev.txt")
    lines = f.readlines()
    f.close()

    TF = []
    IDF = []
    TFIDf = []
    result_split = []
    E_sentences = []
    # 文章を" ||| "で分割
    print(" |||でsplit中" + str(time.time()))
    [E_sentences.append(remove_dust(line.split(" ||| ")[3])) for line in lines]
    # for line in lines:
    #     result_split.append(line.split(" ||| "))
    result_split.sort(key=lambda x:(x[1],x[2]))


    word_List = []
    words_List = []
    #文章1,000,000文　それを分かち書きしてる．時間かかる
    print("わかち書き中" + str(time.time()))
    [words_List.append(sentence.split(" ")) for sentence in E_sentences]
    print("単語配列に入れ中" + str(time.time()))
    [word_List.append(word) for words in words_List for word in words if word not in word_List]
    print("ソート中" + str(time.time()))
    word_List.sort()
    #ファイルに書き込み
    print("書き込み中" + str(time.time()))
    with open("word_List.txt", "w") as file:
        [file.write(word + "\n") for word in word_List]
    print("finish!!!" + str(time.time()))
'''
    print("表示")
    for i in range(0,100):
        print word_List[i]
'''
'''ソート結果表示
    for var in range(1, 100):
        print (result_split[var])

    #TF-IDF計算する
    for item in result_split:
        #論文ID
        id = item[1]
        #Sentence番号
        Sen_id = item[2]
        #英文
        Sen_Eng = item[4].replace(".\n","")
        print Sen_Eng
        #英文を空白で分割
        Sen_Eng_split = Sen_Eng.split(" ")
        #TFIDF: 文字カウント
        #for word in Sen_Eng_split:
'''
