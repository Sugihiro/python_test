#! /usr/bin/env python
#coding:utf-8

import pprint
import json
import corenlp

#ファイル読み込み
f = open("text_set/dev.txt")
line = f.readlines()
f.close()

words_top = ["This work ","This paper ","We ","this work ","this paper", "we ","our method","Our method","the study ","The study"]
results_top=[]
abst_pre =""
#word_searchを含むものを探す．
for l in line:
    results = l.split(" ||| ")
    if abst_pre != results[0]:
        for word in words_top:
            if results[3].find(word) >= 0:
                buf=[results[0],results[1],results[3]]
                results_top.append(buf)
                abst_pre = results[0]
                print results[3]
print str(len(line)) + "    " + str(len(results_top))

#パーサの作成
print "parser make"
corenlp_dir= "./stanford-corenlp-full-2014-10-31"
properties_file = "./user.properties"
parser = corenlp.StanfordCoreNLP(corenlp_path=corenlp_dir, properties=properties_file)
#result_jsonの構造 {sentences:["parsetree", "text", "dependencies", "words", "indexeddependencies"]}
result_json =[]
print "parse start"
for item in results_top:
    sentence = item[2]
    # パースして結果をpretty print
    result_json.append(json.loads(parser.parse(sentence),"utf-8"))
    # pprint.pprint(result_json)

#result_jsonの構造 {sentences:["parsetree", "text", "dependencies", "words", "indexeddependencies"]}

pos_list = ["VBZ", "VBD", "VBG", "VB", "VBN"]
for item in result_json:
    words = item["sentences"][0]["words"]
    root_words = item["sentences"][0]["dependencies"][0][2]
    pprint.pprint(item["sentences"][0])
    for word in words:
        pos = word[1]["PartOfSpeech"]
        lemma = word[1]["Lemma"]
        if(word[0] == root_words):
            print lemma